
# Project Title

 SESAWS Email System

## Authors

- [@manuelcastro23](https://gitlab.com/manuelcastro23)


## Tech Stack

**Client:**  Css3, Html5, Blade, Livewire, Bootstrap 5.

**Server:** PHP 8.2 , Laravel 9.


## Installation

Install my-project with these commands, you need to be insade of the folder of the project:
```bash
  composer install 
  npm install
  php artisan generate:key
  php artisan migrate
  php artisan db:seed --class=UserSeeder
```
You need to create a database called `sesaws_db` in your database management software, in my case I used mysql with phpmyadmin.

Next you need to create an `.env` file and write the values ​​found in the next section    
## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

APP_NAME=SESAWS
APP_ENV=local
APP_KEY=base64:1XrVZ2gyhTgIMojSFmuMdkUkusUL4ih9i62bHo+4rpk=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=sesaws_db
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=database
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=25
MAIL_USERNAME=tester.email.mc@gmail.com
MAIL_PASSWORD=sfebzivfipgydmln 
MAIL_ENCRYPTION=none

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

## Appendix

These are the credentials to access the site, depending on the type of user you choose, the menu options will be shown: for admin user, we have two options user list and email list, for normal user only email list.

ADMINISTRATOR CREDENTIALS

 user: admin@example.com
 pass: password

NORMAL USERS CREDENTIALS

user: usuario1@example.com
pass: password

 user: usuario2@example.com
 pass: password
## Deployment

To deploy this project run

```bash
  php artisan serve
  npm run dev
```
To the queues process, you need to execute this command:

```bash
php artisan queue:work
```
## 🛠 Skills
PHP, Laravel, Javascript, HTML, CSS, Bootstrap 5...


## License

[MIT](https://choosealicense.com/licenses/mit/)

