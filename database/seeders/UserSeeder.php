<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        // Crear el usuario administrador
        $admin = new User();
        $admin->DNI = 0;
        $admin->name = 'Administrador';
        $admin->user_type = User::ADMINISTRATOR;
        $admin->email = 'admin@example.com'; // Puedes cambiar el email del administrador según sea necesario
        $admin->password = Hash::make('password'); // Aquí debes ingresar la contraseña deseada para el administrador

        $admin->save();

           // Crear usuario 1
        $user1 = new User();
        $user1->DNI = '123456789';
        $user1->name = 'usuario1';
        $user1->email = 'usuario1@example.com'; // Cambia el correo electrónico según sea necesario
        $user1->user_type = User::NORMAL_USER; // Asigna el tipo de usuario según sea necesario
        $user1->password = Hash::make('password'); // Cambia la contraseña según sea necesario

        $user1->save();

        // Crear usuario 2
        $user2 = new User();
        $user2->DNI = '987654321';
        $user2->name = 'usuario2';
        $user2->email = 'usuario2@example.com'; // Cambia el correo electrónico según sea necesario
        $user2->user_type = User::NORMAL_USER; // Asigna el tipo de usuario según sea necesario
        $user2->password = Hash::make('password'); // Cambia la contraseña según sea necesario

        $user2->save();
    
    }
}
