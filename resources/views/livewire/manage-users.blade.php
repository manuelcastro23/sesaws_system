<div>
   <div class="container">

   <!-- Button Create User-->
<div class="text-end">
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bootstrapModal" wire:click="modal()">
        Create User
    </button>
</div>
<br>
{{-- USERS LIST --}}
    <livewire:users-table />

{{-- USERS CREATE FORM --}}

  <!-- Modal -->
  <div wire:ignore.self class="modal fade" id="bootstrapModal" tabindex="-1" aria-labelledby="bootstrapModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="bootstrapModalLabel">{{$action}} User</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close_modal"></button>
        </div>
        <div class="modal-body">
            <div class="container">
                <div class="row">

                    <h5>User Data</h5>
                   <hr>
                    <label for="user_type">User Type: <span class="text-danger">(*)</span></label>
                    <select class="form-select" id="user_type" wire:model="user.user_type">
                        <option value="1" selected>Administrator</option>
                        <option value="2">Normal User</option>
                    </select>
                    @error('user.type')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror


                       <label for="user_email">Email: <span class="text-danger">(*)</span></label>
                       <input type="text" class="form-control" id="user_email" wire:model="user.email" autocomplete="off" {{$isEdit ? 'readonly' : ''}}>
                       @error('user.email')
                           <div class="text-danger">{{$message}}</div>
                       @enderror

             
                    @if($isEdit != 1)
                      <label for="password">Password :<span class="text-danger">(*) </span></label>
                      <input type="password" class="form-control" id="password" autocomplete="off" wire:model="user.password">
                      @error('user.password')
                          <div class="text-danger">{{$message}}</div>
                      @enderror
                      
                      <label for="password_confirmation">Password Confirmation:<span class="text-danger">(*) </span></label>
                      <input type="password" class="form-control" id="password_confirmation" autocomplete="off" wire:model="user.password_confirmation">
                      @error('user.password_confirmation')
                      <div class="text-danger">{{$message}}</div>
                      @enderror
                      @endif
                      
                 
                     <h5>Personal Information</h5>
                     <hr>
                    <label for="user_DNI">DNI: <span class="text-danger">(*)</span></label>
                     <input type="text" class="form-control" id="user_DNI" wire:model="user.DNI" {{$isEdit ? 'readonly' : ''}}>
                     @error('user.DNI')
                         <div class="text-danger">{{$message}}</div>
                     @enderror

                    <label for="user_name">Name: <span class="text-danger">(*)</span></label>
                     <input type="text" class="form-control" id="user_name" wire:model="user.name">
                     @error('user.name')
                         <div class="text-danger">{{$message}}</div>
                     @enderror

                     <label for="user_phone">Phone:</label>
                     <input type="text" class="form-control" id="user_phone" wire:model="user.phone">
                     @error('user.phone')
                         <div class="text-danger">{{$message}}</div>
                     @enderror

                     <label for="user_date_birthday">Date Birthday: <span class="text-danger">(*)</span></label>
                     <input  type="date" class="form-control" id="user_date_birthday" wire:model="user.date_birthday">
                     @error('user.date_birthday')
                         <div class="text-danger">{{$message}}</div>
                     @enderror
                     
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" wire:click="{{$isEdit ? 'update()' : 'save()'}}">{{$action}} User</button>
        </div>
      </div>
    </div>
  </div>
  
   </div>
   <script>
      livewire.on('notificate-action', action => {
         document.getElementById('close_modal').click();
      });
      livewire.on('open-modal', () => {
            var myModal = new bootstrap.Modal(document.getElementById('bootstrapModal'));
            myModal.show();
        });


   </script>
</div>
