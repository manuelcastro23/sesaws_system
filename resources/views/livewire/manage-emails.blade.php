<div>
    <div class="container">
 
    <!-- Button Create Email-->
 <div class="text-end">
     <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#bootstrapModal" wire:click="modal()">
         Create Email
     </button>
 </div>
 <br>
 {{-- EMAILS LIST --}}
 <livewire:emails-table />

 {{-- EMAILS CREATE FORM --}}
 
   <!-- Modal -->
   <div wire:ignore.self class="modal fade" id="bootstrapModal" tabindex="-1" aria-labelledby="bootstrapModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <h5 class="modal-title" id="bootstrapModalLabel">{{$action}} Email</h5>
           <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close_modal"></button>
         </div>
         <div class="modal-body">
             <div class="container">
                 <div class="row">

                     <label for="email_subject">Subject: <span class="text-danger">(*)</span></label>
                     <input type="text" class="form-control" id="email_subject" wire:model="email.subject" autocomplete="off">
                     @error('email.subject')
                         <div class="text-danger">{{$message}}</div>
                     @enderror
 
                     <label for="email_recipient">Recipient :<span class="text-danger">(*) </span></label>
                     <input type="text" class="form-control" id="email_recipient" autocomplete="off" wire:model="email.recipient">
                     @error('email.recipient')
                         <div class="text-danger">{{$message}}</div>
                     @enderror
 
                      <label for="email_body">Body: <span class="text-danger">(*)</span></label>
                      <textarea class="form-control" id="email_body" name="email_body" rows="8" wire:model="email.body"></textarea>
                      @error('email.body')
                          <div class="text-danger">{{$message}}</div>
                      @enderror
                      
                 </div>
             </div>
         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
           <button type="button" class="btn btn-primary" wire:click="save()">{{$action}} Email</button>
         </div>
       </div>
     </div>
   </div>
   
    </div>
    <script>
       livewire.on('notificate-action', action => {
          document.getElementById('close_modal').click();
       });
       livewire.on('open-modal', () => {
             var myModal = new bootstrap.Modal(document.getElementById('bootstrapModal'));
             myModal.show();
         });
 
 
    </script>
 </div>
 