@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Welcome, select a Menu Option</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                    <ul>
                        @if(auth()->user()->isAdmin())
                          <li><a href="{{ route('admin.users') }}">Users list</a></li>
                       @endif
                       <li><a href="{{ route('emails') }}">Email list</a></li>


                   </ul>
                    {{-- <livewire:users-table /> --}}
                    
                           {{-- <livewire:datatable
                           model="App\Models\User"
                           searchable="name,email"
                           > --}}
                  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
