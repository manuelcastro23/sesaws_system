<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::prefix('admin')->middleware('auth', 'admin')->group(function () {
   
    Route::get('/users', function () {
        return view('admin.users');
    })->name('admin.users');

    // Route::get('/email', function () {
    //     return view('email');
    // })->name('email');


    // Route::get('/users/{id}/edit', function () {
    //     // return view('users');
    // })->name('admin.users.edit');

    // Route::get('/users/{id}/show', function () {
    //     // return view('users');
    // })->name('admin.users.show');

});

Route::middleware(['auth'])->group(function () {

    Route::get('/emails', function () {
        return view('emails');
    })->name('emails');

    // Route::get('/email/{id}/edit', function () {
    //     // return view('email');
    // })->name('admin.email.edit');

    // Route::get('/email/{id}/show', function () {
    //     // return view('email');
    // })->name('admin.email.show');

});

