<?php

namespace App\Jobs;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ProcessEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $emailId;

    public function __construct($emailId)
    {
        $this->emailId = $emailId;
    }

    public function handle()
    {
        try {
            // Carga el correo electrónico utilizando el ID proporcionado
            $email = Email::findOrFail($this->emailId);

            // Verifica si el estado del correo electrónico es "not_sent"
            if ($email->status == 'not_sent') {
                // Envía el correo electrónico
                $rs = Mail::raw($email->body, function ($message) use ($email) {
                    // Configura el remitente ("From") del correo electrónico
                    $message->from($email->user->email, $email->user->name);
                    // Configura el destinatario y el asunto del correo electrónico
                    $message->to($email->recipient)->subject($email->subject);
                });

                // Actualiza el estado del correo electrónico a "enviado"
                $email->status = 'sent';
                $email->save();
            }
        } catch (\Exception $e) {
            // Maneja cualquier excepción que pueda ocurrir durante el envío del correo electrónico
            // Puedes registrar el error, reintentar el envío, etc.
            // Por ejemplo, puedes agregar una columna de intentos a tu tabla de correos electrónicos
            // y reintentar el envío varias veces antes de marcarlo como fallido.

            // Aquí puedes registrar el error en los registros
            // Log::error('Error al enviar correo electrónico: ' . $e->getMessage());
            dd('Error al enviar correo electrónico: ' . $e->getMessage());
        }
    }

}
