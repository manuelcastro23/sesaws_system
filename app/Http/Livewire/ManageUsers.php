<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ManageUsers extends Component
{
    public User $user;
    public $isEdit = 0;
    public $action = 'Create';
    protected $listeners = ['user-edit' => 'edit','user-deleted' => 'delete'];

    protected $rules = [
        'user.user_type' => 'required',
        'user.DNI' => 'required|integer|max_digits:11',
        'user.name' => 'required|string|max:100',
        'user.phone' => 'nullable|integer|max_digits:10',
        'user.date_birthday' => 'required|date_format:Y-m-d',
        'user.email' => 'required|email|unique:users,email',
        'user.password' => [
            'required',
            'min:8',
            'confirmed',
            'regex:/^(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()\-_=+{};:,<.>ยง~])(?=.*[a-z]).{8,}$/'
        ],
        'user.password_confirmation' => 'required|same:user.password',
    ];

    public function mount(){
        $this->user = new User();
    }

    public function render()
    {
        return view('livewire.manage-users', [
            'users' => User::all()
        ]);
    }
    public function modal($userId = null){
        $this->resetValidation();
        
        if($userId != null){
            $this->isEdit = 1;
            $this->action = 'Update';
            $this->user = User::find($userId);
        }else{
            $this->isEdit = 0;
            $this->action = 'Create';
            $this->user = new User();
            $this->user->user_type = 1;

        }
    }
    public function save(){


        // Define los mensajes de validación personalizados
        $this->rules['user.date_birthday'] = 'required|date_format:Y-m-d|before_or_equal:' . Carbon::now()->subYears(18)->format('Y-m-d');

        $this->validate();

        // Obtengo acceso a la instacia del modelo y sus atributos con valores asignados
        $attributes = $this->user->getAttributes();
        // Quita la propiedad 'password_confirmation' del array de atributos del modelo
        unset($attributes['password_confirmation']);
        // encriptar password
        $attributes['password'] = Hash::make($attributes['password']);
        // guardar modificacion de atributos del modelo
        $this->user->setRawAttributes($attributes);
        // Guarda el modelo
        $this->user->save();
    
        $this->mount();
        $this->emit('refresh-users');
        $this->emit('notificate-action', $this->action);
    }

    public function edit($userId = null) {
        // funcion interna de este componente que maneja parametros a configurar en el modal
        $this->modal($userId);
        // evento que dispara el listener de la vista del componente para disparar Bootstrap Modal
        $this->emit('open-modal'); // Emite un evento para abrir el modal
    }

    public function update() {

        $this->rules['user.email'] = 'required|email';
        $this->rules['user.password'] = '';
        $this->rules['user.password_confirmation'] = '';

         $this->validate();
          // Obtengo acceso a la instacia del modelo y sus atributos con valores asignados
          $attributes = $this->user->getAttributes();
          // Quita la propiedad 'password y password_confirmation' del array de atributos del modelo
          unset($attributes['password']);
          unset($attributes['password_confirmation']);
          // guardar modificacion de atributos del modelo
          $this->user->setRawAttributes($attributes);
          // Guarda el modelo
          $this->user->save();
          
        // Guarda el modelo
        $this->user->save(); // Sincroniza y guarda los datos

        $this->mount();
        $this->emit('refresh-users');
        $this->emit('notificate-action', $this->action);
    }

    public function delete($userId = null) {
        // dd($userId);
            $this->user = User::find($userId);

            $this->user->delete();
            $this->emit('refresh-users'); // Emite un evento para refrescar la tabla de usuarios
    }

}
