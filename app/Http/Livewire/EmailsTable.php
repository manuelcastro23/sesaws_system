<?php

namespace App\Http\Livewire;

use App\Models\Email;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use App\View\ActionColumn;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

class EmailsTable extends DataTableComponent
{
    protected $model = Email::class;
    protected $listeners = ['refresh-emails' => '$refresh'];

    public $columnSearch = [
        'subject' => null,
        'recipient' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id');
    }

    public function builder(): Builder
    {
        // Obtiene el ID del usuario actualmente autenticado
        $userId = Auth::id();

        // Si el usuario no es administrador, retorna la consulta para recuperar los correos electrónicos
        // asociados al usuario actual
        if (!Auth::user()->isAdmin()) {
            return Email::with('user')->where('user_id', $userId);
        }

        // Si el usuario es administrador, retorna la consulta para recuperar todos los correos electrónicos
        return Email::with('user');
    }


    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),
            Column::make("Subject", "subject")
                 ->searchable()    
                 ->sortable(),
            Column::make("Recipient", "recipient")
                 ->searchable()    
                 ->sortable(),
            Column::make("Body", "body")
                ->sortable(),
            Column::make("Status", "status")
                ->sortable(),
            Column::make("User","user.name")
                ->sortable()
                ->format(function ($row) {
                    return $row; // Accede al nombre del usuario a través de la relación cargada
                }),  
        ];
    }

    // public function edit($userId)
    // {
    //    $this->emit('user-edit', $userId);
    // }
    // public function delete($userId)
    // {
    //    $this->emit('user-deleted', $userId);
    // }
}
