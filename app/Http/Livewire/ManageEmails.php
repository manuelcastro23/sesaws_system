<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Email;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Jobs\ProcessEmails;
use Illuminate\Support\Facades\Log;


class ManageEmails extends Component
{
    public Email $email;
    public $action = 'Create';
    // protected $listeners = ['user-edit' => 'edit','user-deleted' => 'delete'];

    protected $rules = [
    'email.subject' => 'required|string',
    'email.recipient' => 'required|email',
    'email.body' => 'required|string',
    ];

    public function mount(){
        $this->email = new Email();
    }
    
    public function render()
    {
        return view('livewire.manage-emails', [
            'emails' => Email::all()
        ]);
    }
    public function modal($emailId = null){
        $this->resetValidation();
            $this->action = 'Send';
            $this->email = new Email();
    }

    public function save(){

        try {
            $this->validate($this->rules);
    
            // se procede a crear el registro en la base de datos 
            // obtengo los atributos del modelo para asignar el user Id
            $attributes = $this->email->getAttributes();
            $attributes['user_id'] = Auth::id();
            $this->email->setRawAttributes($attributes); //para guardar los cambios en atributos del modelo.
            
            // Guarda el modelo
            $this->email->save();
            
            // Se agrega el envio del Email a la cola de Scheduler
            ProcessEmails::dispatch($this->email->id);
        
            $this->mount();
            $this->emit('refresh-emails');
            $this->emit('notificate-action', $this->action);
        } catch (\Exception $e) {
            // Manejo de la excepción
            // Aquí puedes registrar el error, notificar al usuario, etc.
            // Por ejemplo:
            Log::error('Error al guardar y enviar el correo: ' . $e->getMessage());
            // También puedes agregar lógica para revertir cualquier cambio que se haya hecho antes de la excepción.
        }
    }

}