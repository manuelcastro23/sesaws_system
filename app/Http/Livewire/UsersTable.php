<?php

namespace App\Http\Livewire;

use DateTime;
use App\Models\User;
use App\View\ActionColumn;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

class UsersTable extends DataTableComponent
{
    protected $model = User::class;
    protected $listeners = ['refresh-users' => '$refresh'];

    public $columnSearch = [
        'name' => null,
        'email' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        
    }

    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable(),
            Column::make("DNI", "DNI")
            ->sortable(),
            Column::make("Name", "name")
                 ->searchable()    
                 ->sortable(),
            Column::make("Email", "email")
                 ->searchable()    
                 ->sortable(),
            Column::make("Phone", "phone")
                  ->sortable(),
            Column::make("Date Birthday", "date_birthday")
                 ->sortable(),  
            Column::make("Age", "age")
                  ->label(
                      function ($row) {
                          // Calcula la edad basada en la fecha de nacimiento
                          $birthday = new DateTime($row->date_birthday);
                          $today = new DateTime();
                          $age = $today->diff($birthday)->y;
                          return $age;
                      }
                  ),               
           Column::make("User", "user_type")
                  ->sortable()
                  ->format(function ($value) {
                      return User::getTypeDescription($value);
                  }),
            Column::make("Created at", "created_at")
                ->sortable(),
            Column::make('Actions')
                ->label(
                    function ($row, Column $column) {
                        $edit = '<a type="button" class="btn btn-warning" wire:click="edit(' . $row->id . ')">Edit</a>';
                        $delete = '<a type="button" class="btn btn-danger" wire:click="delete(' . $row->id . ')">Delete</a>';
                        return $edit . $delete;
                    }
                )->html(),    
        ];
    }

    public function edit($userId)
    {
       $this->emit('user-edit', $userId);
    }
    public function delete($userId)
    {
       $this->emit('user-deleted', $userId);
    }
}
