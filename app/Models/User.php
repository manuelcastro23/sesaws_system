<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Casts\Attribute;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
   
    // user type
    const ADMINISTRATOR = 1;
    const NORMAL_USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'DNI',
        'name',
        'email',
        'user_type',
        'password',
        'phone',
        'date_birth',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // Helper Functions
    public function isAdmin()
    {
        return $this->user_type === self::ADMINISTRATOR;
    }

    public static function getTypeDescription($type)
    {
        switch ($type) {
            case self::ADMINISTRATOR:
                return 'Administrator';
            case self::NORMAL_USER:
                return 'Normal user';
            default:
                return 'Desconocido';
        }
    }

}
